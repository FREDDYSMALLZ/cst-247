﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BibleVerseApplication.Models;

namespace BibleVerseApplication.Controllers
{

    public class TestController : Controller
    {
        // GET: Test
        BibleDataBaseEntities dc = new BibleDataBaseEntities();
        public ActionResult Index()
        {
            List<TestamentTable> TestamentList = dc.TestamentTables.ToList();
            ViewBag.TestamentList = new SelectList(TestamentList, "TestamentId", "Name");
            return View();
        }

        public JsonResult GetOldBooksList(int TestamentId)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            List<OldTestamentTable> OldTestamentList =
                dc.OldTestamentTables.Where(x => x.TestamentId == TestamentId).ToList();
            return Json(OldTestamentList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetBooksList(int TestamentId)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            List<NewTestamentTable> NewTestamentList =
                dc.NewTestamentTables.Where(x => x.TestamentId == TestamentId).ToList();
            return Json(NewTestamentList, JsonRequestBehavior.AllowGet);

        }
    }
}