﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using BibleVerseApplication.Models;
namespace BibleVerseApplication.Controllers
{
    public class BibleVerseController : Controller
    {
        private MyBibleDatabaseEntities db = new MyBibleDatabaseEntities();
 
        // GET: BibleVerse
        public ActionResult VerseListing()
        {
            return View(db.BibleTables.ToList());
        }

        // GET: BibleVerse/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleTable bibleTable = db.BibleTables.Find(id);
            if (bibleTable == null)
            {
                return HttpNotFound();
            }
            return View(bibleTable);
        }

        // GET: BibleVerse/Create
        BibleDataBaseEntities dc = new BibleDataBaseEntities();
        public ActionResult Create()
        {
            List<TestamentTable> TestamentList = dc.TestamentTables.ToList();
            ViewBag.TestamentList = new SelectList(TestamentList, "TestamentId", "Name");

            return View();
        }
        public JsonResult GetOldBooksList(int TestamentId)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            List<OldTestamentTable> OldTestamentList =
                dc.OldTestamentTables.Where(x => x.TestamentId == TestamentId).ToList();
            return Json(OldTestamentList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetBooksList(int TestamentId)
        {
            dc.Configuration.ProxyCreationEnabled = false;
            List<NewTestamentTable> NewTestamentList =
                dc.NewTestamentTables.Where(x => x.TestamentId == TestamentId).ToList();
            return Json(NewTestamentList, JsonRequestBehavior.AllowGet);

        }


        // POST: BibleVerse/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BibleId,TestamentSelection,BookSelection,ChapterNumber,VerseNumber,VerseText,DateOfEntry")] BibleTable bibleTable)
        {
            if (ModelState.IsValid)
            {
                db.BibleTables.Add(bibleTable);
                db.SaveChanges();
                return RedirectToAction("VerseListing");
            }

            return View(bibleTable);
        }

        // GET: BibleVerse/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleTable bibleTable = db.BibleTables.Find(id);
            if (bibleTable == null)
            {
                return HttpNotFound();
            }
            return View(bibleTable);
        }

        // POST: BibleVerse/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BibleId,TestamentSelection,BookSelection,ChapterNumber,VerseNumber,VerseText,DateOfEntry")] BibleTable bibleTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bibleTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("VerseListing");
            }
            return View(bibleTable);
        }

        // GET: BibleVerse/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleTable bibleTable = db.BibleTables.Find(id);
            if (bibleTable == null)
            {
                return HttpNotFound();
            }
            return View(bibleTable);
        }

        // POST: BibleVerse/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BibleTable bibleTable = db.BibleTables.Find(id);
            db.BibleTables.Remove(bibleTable);
            db.SaveChanges();
            return RedirectToAction("VerseListing");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
