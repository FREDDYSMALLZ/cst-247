﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BibleVerseApplication.Models;

namespace BibleVerseApplication.Controllers
{
    public class VerseSearchController : Controller
    {
        private MyBibleDatabaseEntities db = new MyBibleDatabaseEntities();

        // GET: VerseSearch
        public ActionResult Search(string searchBy, string search)
        {
            if (searchBy == "Testament")
            {
                return View(db.BibleTables.Where(x => x.TestamentSelection == search || search == null).ToList());
            }
            else
            {
                return View(db.BibleTables.Where(x => x.BookSelection.StartsWith(search) || search == null).ToList());
            }
        }

        // GET: VerseSearch/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleTable bibleTable = db.BibleTables.Find(id);
            if (bibleTable == null)
            {
                return HttpNotFound();
            }
            return View(bibleTable);
        }
        // GET: VerseSearch/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleTable bibleTable = db.BibleTables.Find(id);
            if (bibleTable == null)
            {
                return HttpNotFound();
            }
            return View(bibleTable);
        }

        // POST: VerseSearch/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BibleId,TestamentSelection,BookSelection,ChapterNumber,VerseNumber,VerseText,DateOfEntry")] BibleTable bibleTable)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bibleTable).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Search");
            }
            return View(bibleTable);
        }

        // GET: VerseSearch/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BibleTable bibleTable = db.BibleTables.Find(id);
            if (bibleTable == null)
            {
                return HttpNotFound();
            }
            return View(bibleTable);
        }

        // POST: VerseSearch/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BibleTable bibleTable = db.BibleTables.Find(id);
            db.BibleTables.Remove(bibleTable);
            db.SaveChanges();
            return RedirectToAction("Search");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
