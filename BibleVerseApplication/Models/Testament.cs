﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibleVerseApplication.Models
{
    public class Testament
    {
        public int TestamentId { get; set; }
        public string Name { get; set; }
    }
}