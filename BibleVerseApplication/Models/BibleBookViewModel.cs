﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibleVerseApplication.Models
{
    public class BibleBookViewModel
    {
        public int TestamentId { get; set; }
        public int NewId { get; set; }
        public int OldId { get; set; }
    }
}