using System.ComponentModel.DataAnnotations;

namespace BibleVerseApplication.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BibleTable
    {
        [Key]
        [Display(Name = "Bible Id Number")]
        public int BibleId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Select your Bible Testament")]
        [Display(Name = "Testament")]
        public string TestamentSelection { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Select your Book from the Bible")]
        [Display(Name = "Bible Book")]
        public string BookSelection { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [Display(Name = "Chapter Number")]
        [Range(1, 157)]
        public int ChapterNumber { get; set; }


        [Display(Name = "Verse Number")]
        [Range(1, 157)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter the verse Number")]
        public int VerseNumber { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter the Book Verse")]
        [Display(Name = "Verse Text")]
        [DataType(DataType.MultilineText)]
        [StringLength(2000)]
        public string VerseText { get; set; }

        [Required]
        [Display(Name = "Date of Verse Entry")]
        [DataType(DataType.Date)]
        public DateTime DateOfEntry { get; set; }

        public int TestamentId { get; set; }
        public int NewId { get; set; }
        public int OldId { get; set; }
    }
}
