﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibleVerseApplication.Models
{
    public class OldTest
    {
        public int OldId { get; set; }
        public string Name { get; set; }
        public int TestamentId { get; set; }
    }
}